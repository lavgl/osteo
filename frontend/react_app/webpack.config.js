const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const sourcePath = path.join(__dirname, './src');
const buildPath = path.join(__dirname, './build');
const publicPath = path.join(__dirname, './public');

const adminBuildPath = path.join(buildPath, './admin');
const agentBuildPath = path.join(buildPath, './agent');


const env = process.env.NODE_ENV || 'development';
const role = process.env.FOR_ROLE || 'both';

const isProduction = env === 'production';

const plugins = [
  new webpack.optimize.CommonsChunkPlugin({
    names: ['common', 'vendor'],
    minChunks: 2
  }),
  new webpack.EnvironmentPlugin({
    NODE_ENV: env
  }),
  new webpack.NamedModulesPlugin()
];

const rules = [
  {
    enforce: 'pre',
    test: /\.js$/,
    exclude: /node_modules/,
    include: sourcePath,
    loader: 'eslint-loader'
  },
  {
    test: /\.js$/,
    exclude: /node_modules/,
    loader: 'babel-loader'
  },
  {
    test: /\.(png|gif|jpg|svg)$/,
    include: publicPath,
    use: 'url-loader?limit=20480&name=assets/[name]-[hash].[ext]'
  }
];

if (isProduction) {
  plugins.push(
    new ExtractTextPlugin('style-[chunkhash].css'),
    new UglifyJSPlugin()
  );

  rules.push({
    test: /\.css$/,
    loader: ExtractTextPlugin.extract({
      fallback: 'style-loader',
      use: 'css-loader'
    })
  });
} else {
  plugins.push(
    new webpack.HotModuleReplacementPlugin()
  );

  rules.push({
    test: /\.css$/,
    exclude: /node_modules/,
    use: [
      'style-loader',
      'css-loader'
    ]
  })
}

const baseConfig = {
  entry: {
    vendor: [
      'react',
      'react-dom',
      'redux',
      'react-redux',
      'react-router',
      'react-router-redux',
      'redux-thunk',
      'reselect',
      'immutable',
      'material-ui',
      'normalizr',
      'react-tap-event-plugin',
      'isomorphic-fetch'
    ]
  },
  output: {
    filename: '[name]-[hash].js',
    publicPath: '/'
  },
  context: sourcePath,
  devServer: {
    hot: true,
    inline: true,
    port: 3000,
    stats: 'minimal'
  },
  module: {
    rules
  },
  resolve: {
    alias: {
      components: path.resolve(__dirname, 'src/components/'),
      actions: path.resolve(__dirname, 'src/redux/actions/'),
      reducers: path.resolve(__dirname, 'src/redux/reducers/'),
      selectors: path.resolve(__dirname, 'src/redux/selectors'),
      pages: path.resolve(__dirname, 'src/pages/'),
      utils: path.resolve(__dirname, 'src/utils/'),
      decorators: path.resolve(__dirname, 'src/decorators/')
    }
  },
  plugins
};

const adminConfig = merge(baseConfig, {
  name: 'admin',
  entry: {
    js: './index.admin.js'
  },
  output: {
    path: adminBuildPath
  },
  devServer: {
    contentBase: adminBuildPath
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: 'Admin Page',
      template: path.join(sourcePath, 'index.ejs'),
      path: adminBuildPath,
      inject: false
    })
  ]
});

const agentConfig = merge(baseConfig, {
  name: 'agent',
  entry: {
    js: './index.agent.js'
  },
  output: {
    path: agentBuildPath
  },
  devServer: {
    contentBase: agentBuildPath
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: 'Agent Page',
      template: path.join(sourcePath, 'index.ejs'),
      path: agentBuildPath,
      inject: false
    })
  ]
});

const _exports = {
  admin: adminConfig,
  agent: agentConfig,
  both: [ adminConfig, agentConfig ]
};

module.exports = _exports[role];