import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import Immutable from 'immutable';

import { UserInfoForm } from '../components/UserInfoForm';

import { saveUser } from '../redux/actions/Users';
import { showNotification } from '../redux/actions/Notifications';

const initialState = {
  user: Immutable.Map()
};

const mapDispatchToProps = {
  saveUser,
  showNotification
};

export default function createUserPageFactory({ role }) {
  @connect(null, mapDispatchToProps)
  class CreateUserPage extends Component {
    static propTypes = {
      saveUser: PropTypes.func.isRequired,
      showNotification: PropTypes.func.isRequired
    };

    constructor(props) {
      super(props);

      this.state = initialState;

      this.handleSubmit = this.handleSubmit.bind(this);
      this.handleReset = this.handleReset.bind(this);
      this.handleFieldChange = this.handleFieldChange.bind(this);
    }

    handleSubmit(e) {
      e.preventDefault();
      const { user } = this.state;
      this.props.saveUser(null, user.set('role', role))
        .then(result => {
          const message = result.error
            ? 'Error while creating'
            : 'Successfully created';
          this.props.showNotification(message);

          if (!message.error) {
            const id = result.response.result[0];
            const plural = role.toLowerCase().concat('s');
            browserHistory.push(`/${plural}/${id}`);
          }
        });
    }

    handleReset() {
      this.setState(initialState);
    }

    handleFieldChange(field, e) {
      const { user } = this.state;
      const { value } = e.target;
      this.setState({ user: user.set(field, value) });
    }

    render() {
      return (
        <div style={{ padding: 20 }}>
          <h1>Add new {role}</h1>
          <UserInfoForm
            user={this.state.user}
            onFieldChange={this.handleFieldChange}
            onSubmit={this.handleSubmit}
            onReset={this.handleReset}
          />
        </div>
      );
    }
  }

  return CreateUserPage;
}