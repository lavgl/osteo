import React, { Component, PropTypes } from 'react';
import Immutable from 'immutable';

import RaisedButton from 'material-ui/RaisedButton';

import { FilterTable } from 'components/Table';

import I18n from 'decorators/I18n';

import { agentTableColumns } from '../../helpers/columns';

@I18n('agentsListPage')
class AgentsListPageView extends Component {
  static propTypes = {
    agents: PropTypes.instanceOf(Immutable.Map).isRequired,
    clients: PropTypes.instanceOf(Immutable.Map).isRequired,
    payments: PropTypes.instanceOf(Immutable.Map).isRequired,
    deleteUser: PropTypes.func.isRequired,
    showNotification: PropTypes.func.isRequired,
    handleAddNew: PropTypes.func.isRequired,
    t: PropTypes.func.isRequired
  };

  render() {
    const { t } = this.props;
    return (
      <div style={{ padding: 20 }}>
        <h1>{t('title')}</h1>
        <RaisedButton
          label = {t('buttonLabel')}
          primary={true}
          onClick={this.props.handleAddNew}
        />
        <FilterTable
          columns={agentTableColumns}
          data={this.props.agents.toList()}
          clients={this.props.clients}
          payments={this.props.payments}
          deleteUser={this.props.deleteUser}
          showNotification={this.props.showNotification}
        />
      </div>
    )
  }
}

export default AgentsListPageView;