import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import Immutable from 'immutable';

import AgentsListPageView from './AgentsListPageView';

import { loadUsers, deleteUser } from '../../redux/actions/Users';
import { loadPayments } from '../../redux/actions/Payments';
import { showNotification } from '../../redux/actions/Notifications';

import { agents, clients, payments } from '../../redux/selectors';

function mapStateToProps(state) {
  return {
    agents: agents(state),
    clients: clients(state),
    payments: payments(state)
  };
}

const mapDispatchToProps = {
  loadUsers,
  deleteUser,
  loadPayments,
  showNotification,
};

@connect(mapStateToProps, mapDispatchToProps)
class AgentsListPage extends Component {
  static propTypes = {
    agents: PropTypes.instanceOf(Immutable.Map).isRequired,
    clients: PropTypes.instanceOf(Immutable.Map).isRequired,
    payments: PropTypes.instanceOf(Immutable.Map).isRequired,
    loadUsers: PropTypes.func.isRequired,
    deleteUser: PropTypes.func.isRequired,
    loadPayments: PropTypes.func.isRequired,
    showNotification: PropTypes.func.isRequired
  };

  componentWillMount() {
    this.props.loadPayments();
  }

  componentWillReceiveProps(nextProps) {
    console.log('agents list page will receive props');
    console.log('current', this.props, 'next', nextProps);
  }

  shouldComponentUpdate() {
    console.log('agent list page updates');
    return true;
  }

  handleAddNew() {
    browserHistory.push('/agents/new');
  }

  render() {
    return (
      <AgentsListPageView
        agents = {this.props.agents}
        clients = {this.props.clients}
        payments = {this.props.payments}

        handleAddNew = {this.handleAddNew}
        deleteUser = {this.props.deleteUser}

        showNotification = {this.props.showNotification}
      />
    )
  }
}

export default AgentsListPage;