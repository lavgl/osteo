import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import Immutable from 'immutable';

import DatePicker from 'material-ui/DatePicker';
import TimePicker from 'material-ui/TimePicker';
import RaisedButton from 'material-ui/RaisedButton';

import EditableInput from '../components/EditableInput';

import { FilterTable } from '../components/Table';
import { clientTableColumns, paymentsTableColumns } from '../helpers/columns';

import { loadUser, saveUser, loadUsers, deleteUser } from '../redux/actions/Users';
import { showNotification } from '../redux/actions/Notifications';
import { loadPayments, savePayment, deletePayment } from '../redux/actions/Payments';
import {
  user,
  makeClientsForAgent,
  makePaymentsForAgent
} from '../redux/selectors';

import { UserInfoForm } from '../components/UserInfoForm';

function mapStateToProps(state, props) {
  const clientsForAgent = makeClientsForAgent();
  const paymentsForAgent = makePaymentsForAgent();
  return {
    agent: user(state, props),
    clients: clientsForAgent(state, props),
    payments: paymentsForAgent(state, props)
  };
}

const mapDispatchToProps = {
  loadUser,
  saveUser,
  showNotification,
  loadUsers,
  loadPayments,
  savePayment,
  deleteUser,
  deletePayment
};

@connect(mapStateToProps, mapDispatchToProps)
class AgentInfoPage extends Component {
  static propTypes = {
    agent: PropTypes.instanceOf(Immutable.Map).isRequired,
    clients: PropTypes.instanceOf(Immutable.Map).isRequired,
    payments: PropTypes.instanceOf(Immutable.Map).isRequired,
    loadUser: PropTypes.func.isRequired,
    saveUser: PropTypes.func.isRequired,
    loadUsers: PropTypes.func.isRequired,
    showNotification: PropTypes.func.isRequired,
    loadPayments: PropTypes.func.isRequired,
    savePayment: PropTypes.func.isRequired,
    deleteUser: PropTypes.func.isRequired,
    deletePayment: PropTypes.func.isRequired,
    params: PropTypes.object
  };

  constructor(props) {
    super(props);

    this.state = {};

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleFieldChange = this.handleFieldChange.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.handlePaymentDateChange = this.handlePaymentDateChange.bind(this);
    this.handleCreateNewPayment = this.handleCreateNewPayment.bind(this);

    paymentsTableColumns[1].render = (datum) => {
      return (
        <EditableInput
          hintText="Amount"
          value={datum.get('amount')}
          type="number"
          onSubmit={this.handlePaymentFieldChange.bind(this, datum, 'amount')}
        />
      )
    };

    paymentsTableColumns[2].render = (datum) => {
      return (
        <DatePicker
          hintText="Date"
          value={new Date(datum.get('date'))}
          onChange={this.handlePaymentDateChange.bind(this, datum)}
          autoOk={true}
        />
      );
    };

    paymentsTableColumns[3].render = (datum) => {
      return (
        <TimePicker
          hintText="Time"
          format="24hr"
          value={new Date(datum.get('date'))}
          onChange={this.handlePaymentDateChange.bind(this, datum)}
        />
      )
    }
  }

  componentWillMount() {
    const { id } = this.props.params;
    if (this.props.agent) {
      this.setState({ agent: this.props.agent });
    } else {
      this.props.loadUser(id);
    }

    this.props.loadUsers({ agent_id: id });
    this.props.loadPayments({ agent_id: id });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.agent && !this.props.agent) {
      this.setState({ agent: nextProps.agent });
    }
  }

  handleSubmit(e) {
    e.preventDefault();
    const { agent } = this.state;

    this.props.saveUser(agent.get('id'), agent)
      .then(res => {
        this.props.showNotification(res.error
          ? 'Error when updating'
          : 'Successfully updated');
      });
  }

  handleReset() {
    this.setState({ agent: this.props.agent });
  }

  handleFieldChange(field, e) {
    const { agent } = this.state;
    const { value } = e.target;
    this.setState({ agent: agent.set(field, value )});
  }

  handlePaymentDateChange(datum, e, date) {
    const payment = datum.set('date', date).toJS();
    this.props.savePayment(datum.get('id'), payment)
      .then(result => {
        this.props.showNotification(result.error
          ? 'Error'
          : 'Payment date successfully updated')
      });
  }

  handlePaymentFieldChange(datum, field, nextValue, value) {
    if (nextValue === value) {
      return;
    }

    const payment = datum.set(field, nextValue).toJS();
    this.props.savePayment(datum.get('id'), payment)
      .then(result => {
        this.props.showNotification(result.error
          ? 'Error'
          : 'Payment successfully updated')
      });
  }

  handleCreateNewPayment() {
    const payment = {
      amount: 300,
      date: new Date(),
      agent_id: this.props.agent.get('id')
    };

    this.props.savePayment(null, payment)
      .then(result => {
        this.props.showNotification(result.error
          ? 'Error'
          : 'Payment successfully created')
      });
  }

  render() {
    const { agent } = this.props;
    return agent ? (
      <div style={{ padding: 20}}>
        <h1>Agent {agent.get('name')} (id {agent.get('id')}) page</h1>
        <UserInfoForm
          user={this.state.agent}
          onFieldChange={this.handleFieldChange}
          onSubmit={this.handleSubmit}
          onReset={this.handleReset}
        />
        <div>
          <h3>Payments List</h3>
          <RaisedButton
            label="Add payment"
            primary={true}
            onClick={this.handleCreateNewPayment}
          />
          <FilterTable
            data={this.props.payments.toList()}
            columns={paymentsTableColumns}
            deletePayment={this.props.deletePayment}
            showNotification={this.props.showNotification}
          />
        </div>
        <div>
          <h3>Clients List</h3>
          <FilterTable
            data={this.props.clients.toList()}
            columns={clientTableColumns}
            deleteUser={this.props.deleteUser}
            showNotification={this.props.showNotification}
          />
        </div>
      </div>
    ) : (
      <h1>Loading...</h1>
    );
  }
}

export default AgentInfoPage;