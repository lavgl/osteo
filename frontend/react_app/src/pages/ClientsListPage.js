import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import Immutable from 'immutable';

import RaisedButton from 'material-ui/RaisedButton';
import { FilterTable } from '../components/Table';
import { clientTableColumns } from '../helpers/columns';

import { loadVisits } from '../redux/actions/Visits';
import { deleteUser } from '../redux/actions/Users';
import { showNotification } from '../redux/actions/Notifications';

import { clients, visits } from '../redux/selectors';

function mapStateToProps(state) {
  return {
    clients: clients(state),
    visits: visits(state)
  };
}

const mapDispatchToProps = {
  loadVisits,
  deleteUser,
  showNotification
};

@connect(mapStateToProps, mapDispatchToProps)
class ClientsListPage extends Component {
  static propTypes = {
    clients: PropTypes.instanceOf(Immutable.Map).isRequired,
    visits: PropTypes.instanceOf(Immutable.Map).isRequired,
    loadVisits: PropTypes.func.isRequired,
    deleteUser: PropTypes.func.isRequired,
    showNotification: PropTypes.func.isRequired
  };

  componentWillMount() {
    this.props.loadVisits();
  }

  handleAddNew() {
    browserHistory.push('/clients/new');
  }

  render() {
    return (
      <div style={{ padding: 20 }}>
        <h1>Clients list</h1>
        <RaisedButton
          label="add new client"
          primary={true}
          onClick={this.handleAddNew}
        />
        <FilterTable
          columns={clientTableColumns}
          data={this.props.clients.toList()}
          visits={this.props.visits}

          deleteUser={this.props.deleteUser}
          showNotification={this.props.showNotification}
        />
      </div>
    );
  }
}

export default ClientsListPage;