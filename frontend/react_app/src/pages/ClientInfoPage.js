import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import Immutable from 'immutable';

import RaiseButton from 'material-ui/RaisedButton';
import DatePicker from 'material-ui/DatePicker';
import TimePicker from 'material-ui/TimePicker';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

import { loadUser, saveUser } from '../redux/actions/Users';
import { loadVisits, saveVisit, deleteVisit } from '../redux/actions/Visits';
import { user, makeVisitsForClient, agents } from '../redux/selectors';
import { showNotification } from '../redux/actions/Notifications';

import { UserInfoForm } from '../components/UserInfoForm';
import { FilterTable } from '../components/Table';

import { visitsTableColumns} from '../helpers/columns';

function mapStateToProps(state, props) {
  const visitsForClient = makeVisitsForClient();
  return {
    client: user(state, props),
    visits: visitsForClient(state, props),
    agents: agents(state, props)
  };
}

const mapDispatchToProps = {
  loadUser,
  saveUser,
  loadVisits,
  showNotification,
  saveVisit,
  deleteVisit
};

@connect(mapStateToProps, mapDispatchToProps)
class ClientInfoPage extends Component {
  static propTypes = {
    client: PropTypes.instanceOf(Immutable.Map).isRequired,
    visits: PropTypes.instanceOf(Immutable.Map).isRequired,
    agents: PropTypes.instanceOf(Immutable.Map).isRequired,
    loadUser: PropTypes.func.isRequired,
    saveUser: PropTypes.func.isRequired,
    loadVisits: PropTypes.func.isRequired,
    showNotification: PropTypes.func.isRequired,
    saveVisit: PropTypes.func.isRequired,
    deleteVisit: PropTypes.func.isRequired,
    params: PropTypes.object
  };

  constructor(props) {
    super(props);

    this.state = {};

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleFieldChange = this.handleFieldChange.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.handleVisitDateChange = this.handleVisitDateChange.bind(this);
    this.handleAddNewVisit = this.handleAddNewVisit.bind(this);
    this.handleSelectAgent = this.handleSelectAgent.bind(this);

    // TODO: refactor. extract in helpers folder
    visitsTableColumns[1].render = (datum) => {
      return (
        <DatePicker
          name="Date"
          value={new Date(datum.get('date'))}
          onChange={this.handleVisitDateChange.bind(this, datum)}
          autoOk={true}
        />
      );
    };

    visitsTableColumns[2].render = (datum) => {
      return (
        <TimePicker
          format="24hr"
          value={new Date(datum.get('date'))}
          onChange={this.handleVisitDateChange.bind(this, datum)}
        />
      );
    };
  }

  componentWillMount() {
    const { id } = this.props.params;
    if (this.props.client) {
      this.setState({ client: this.props.client });
    } else {
      this.props.loadUser(id);
    }
    this.props.loadVisits({ client_id: id });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.client && !this.props.client) {
      this.setState({ client: nextProps.client });
    }
  }

  handleSubmit(e) {
    e.preventDefault();
    const { client } = this.state;
    this.props.saveUser(client.get('id'), client)
      .then(result => {
        this.props.showNotification(result.error
          ? 'Error'
          : 'Successfully updated');
      });
  }

  handleReset() {
    this.setState({ client: this.props.client });
  }

  handleFieldChange(field, e) {
    const { client } = this.state;
    const { value } = e.target;
    this.setState({ client: client.set(field, value) });
  }

  handleVisitDateChange(datum, e, date) {
    const visit = datum.set('date', date).toJS();
    this.props.saveVisit(datum.get('id'), visit)
      .then(result => {
        this.props.showNotification(result.error
          ? 'Error'
          : 'Date successfully updated');
      });
  }

  handleAddNewVisit() {
    const visit = {
      client_id: this.props.client.get('id'),
      date: new Date()
    };

    this.props.saveVisit(null, visit)
      .then(result => {
        this.props.showNotification(result.error
          ? 'Error'
          : 'Visit with current time successfully added');
      });
  }

  handleSelectAgent(e, i, value) {
    this.setState({ client: this.state.client.set('agent_id', value)});
  }

  render() {
    const { client, visits, agents } = this.props;

    const menuItems = agents.toArray().map(a => {
      return (
        <MenuItem
          key={a.get('id')}
          value={a.get('id')}
          primaryText={a.get('name')}
        />
      );
    });

    const additionalFields = [
      <SelectField
        floatingLabelText="Agent"
        value={this.state.client && this.state.client.get('agent_id')}
        onChange={this.handleSelectAgent}
      >
        {menuItems}
      </SelectField>
    ];

    return client && visits ? (
      <div style={{ padding: 20 }}>
        <h1>Client {client.get('name')} (id {client.get('id')}) page</h1>
        <UserInfoForm
          user={this.state.client}
          onFieldChange={this.handleFieldChange}
          onSubmit={this.handleSubmit}
          onReset={this.handleReset}
          additionalFields={additionalFields}
        />
        <h3>Client's visits</h3>
        <RaiseButton
          label="Add visit"
          primary={true}
          onClick={this.handleAddNewVisit}
        />
        <FilterTable
          data={visits.toList()}
          columns={visitsTableColumns}
          deleteVisit={this.props.deleteVisit}
          showNotification={this.props.showNotification}
        />
      </div>
    ) : (
      <h1>Loading...</h1>
    )
  }
}

export default ClientInfoPage;