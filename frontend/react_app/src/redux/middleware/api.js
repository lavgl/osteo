import 'isomorphic-fetch';
import { normalize } from 'normalizr';

import config from '../../../../../config';

const { host, port } = config().backend.server;

const PROTOCOL = 'http';
const SERVER_HOSTNAME = host;
const SERVER_PORT = port;
const API_PATH = 'api/v1';

const API_ROOT = `${PROTOCOL}://${SERVER_HOSTNAME}:${SERVER_PORT}/${API_PATH}`;

const callApi = (endpoint, schema, options = null) => {
  const fullUrl = (endpoint.indexOf(API_ROOT) === -1) ? API_ROOT + endpoint : endpoint;

  return fetch(fullUrl, options).then(response =>
      response.json().then(json => {
        if (!response.ok) {
          return Promise.reject(json);
        }

        return normalize(json, schema);
      }));
};

export const CALL_API = Symbol('CALL API');

export default store => next => action => {
  const callAPI = action[CALL_API];

  if (typeof callAPI === 'undefined') {
    return next(action);
  }

  let { endpoint } = callAPI;
  const { schema, types, options } = callAPI;

  if (typeof endpoint === 'function') {
    endpoint = endpoint(store.getState());
  }

  if (typeof endpoint !== 'string') {
    throw new Error('Specify a string endpoint URL');
  }

  if (!schema) {
    throw new Error('Specify one of available Schemas');
  }

  if (!Array.isArray(types) || types.length !== 3) {
    throw new Error('Expected an array of three action types');
  }

  if (!types.every(type => typeof type === 'string')) {
    throw new Error('Expected action types to be string');
  }

  const actionWith = data => {
    const finalAction = Object.assign({}, action, data);
    delete finalAction[CALL_API];
    return finalAction;
  }

  const [ requestType, successType, failureType ] = types;
  next(actionWith({ type: requestType }));

  return callApi(endpoint, schema, options).then(
    response => next(actionWith({
      response,
      type: successType
    })),
    error => next(actionWith({
      type: failureType,
      error: error.message || 'Something bad happened'
    }))
  )
};