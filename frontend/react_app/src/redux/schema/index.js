import { Schema, arrayOf } from 'normalizr';

const payment = new Schema('payments');
const visit = new Schema('visits');
const user = new Schema('users');
const status = new Schema('status');

const users = arrayOf(user);
const visits = arrayOf(visit);
const payments = arrayOf(payment);

export {
  user as USER_SCHEMA,
  users as USER_LIST_SCHEMA,
  visits as VISIT_LIST_SCHEMA,
  payments as PAYMENT_LIST_SCHEMA,
  status as STATUS_SCHEMA
};