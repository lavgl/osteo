import { createSelector } from 'reselect';

const users = (state) => state.Entities.get('users');
export const clients = createSelector([users], users, (users) => users.filter(u => u.get('role') === 'Client'));
export const agents = createSelector([users], users, (users) => users.filter(u => u.get('role') === 'Agent'));
export const visits = (state) => state.Entities.get('visits');
export const payments = (state) => state.Entities.get('payments');

export const user = (state, props) => state.Entities.getIn(['users', props.params.id]);

export const makeClientsForAgent = () => {
  return createSelector(
    [user, clients],
    (user, clients) => {
      return clients.filter(c => c.get('agent_id') === user.get('id'));
    }
  );
};

export const makeVisitsForClient = () => {
  return createSelector(
    [user, visits],
    (user, visits) => {
      if (!user) return null;
      return visits.filter(v => v.get('client_id') === user.get('id'));
    }
  );
};

export const makePaymentsForAgent = () => {
  return createSelector(
    [user, payments],
    (user, payments) => {
      if (!user) return null;
      // TODO: check agentId props vs agent_id
      // TODO: check returned null vs Immutable.List()
      return payments.filter(p => p.get('agent_id') === user.get('id'));
    }
  )
};