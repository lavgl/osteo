import Immutable from 'immutable';

import {
  USERS_SUCCESS,
  USER_SUCCESS,
  DELETE_USER_SUCCESS
} from '../actions/Users';

import {
  GET_VISITS_SUCCESS,
  PUT_VISIT_SUCCESS,
  DELETE_VISIT_SUCCESS
} from '../actions/Visits';

import {
  GET_PAYMENTS_SUCCESS,
  PUT_PAYMENT_SUCCESS,
  POST_PAYMENT_SUCCESS,
  DELETE_PAYMENT_SUCCESS
} from '../actions/Payments';

const initialState = Immutable.fromJS({
  visits: {},
  payments: {},
  users: {}
});

const Entities = (state = initialState, action = {}) => {
  switch (action.type) {
    case USERS_SUCCESS:
    case USER_SUCCESS:
    case GET_VISITS_SUCCESS:
    case PUT_VISIT_SUCCESS:
    case GET_PAYMENTS_SUCCESS:
    case PUT_PAYMENT_SUCCESS:
    case POST_PAYMENT_SUCCESS: {
      return state.mergeDeep(Immutable.fromJS(action.response.entities));
    }

    case DELETE_VISIT_SUCCESS: {
      return state.deleteIn(['visits', ''+action.response.result]);
    }

    case DELETE_USER_SUCCESS: {
      return state.deleteIn(['users', ''+action.response.result]);
    }

    case DELETE_PAYMENT_SUCCESS: {
      return state.deleteIn(['payments', ''+action.response.result]);
    }

    default: return state;
  }
};

export default Entities;