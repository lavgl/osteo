import { routerReducer as routing } from 'react-router-redux';
import { combineReducers } from 'redux';

import Entities from './Entities';
import Notifications from './Notifications';

const rootReducer = combineReducers({
  Entities,
  Notifications,
  routing
});

export default rootReducer;