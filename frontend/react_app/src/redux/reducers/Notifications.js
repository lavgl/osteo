import Immutable from 'immutable';

import {
  SHOW_NOTIFICATION,
  HIDE_NOTIFICATION
} from '../actions/Notifications';

const initialState = Immutable.fromJS({
  notifications: []
});

const Notifications = (state = initialState, action = {}) => {
  switch (action.type) {
    case SHOW_NOTIFICATION: {
      return state.setIn(['notifications', 0], action.message);
    }
    case HIDE_NOTIFICATION: {
      return state.set('notifications', Immutable.List());
    }
    default: return state;
  }
};

export default Notifications;