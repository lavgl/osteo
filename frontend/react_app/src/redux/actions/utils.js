import { CALL_API } from '../middleware/api';

export const convertObjectToWhere = (object) => {
  const string = JSON.stringify(object);
  const step1 = string.slice(1, -1).toString();
  const step2 = step1.replace(/"/g, '');
  return `"${step2}"`;
};

export const fetchAllFactory = (types, base, schema) => (where) => ({
  [CALL_API]: {
    types,
    endpoint: `${base}${where
      ? `?where=${convertObjectToWhere(where)}`
      : ''}`,
    schema
  }
});

export const fetchOneFactory = (types, base, schema) => (id) => ({
  [CALL_API]: {
    types,
    endpoint: `${base}/${id}`,
    schema
  }
});

export const putFactory = (types, base, schema) => (id, body) => ({
  [CALL_API]: {
    types,
    endpoint: `${base}/${id}`,
    options: {
      method: 'PUT',
      body: JSON.stringify(body),
      headers: {
        'Content-Type': 'application/json'
      }
    },
    schema
  }
});

export const postFactory = (types, base, schema) => (body) => ({
  [CALL_API]: {
    types,
    endpoint: base,
    options: {
      method: 'POST',
      body: JSON.stringify(body),
      headers: {
        'Content-Type': 'application/json'
      }
    },
    schema
  }
});

export const deleteFactory = (types, base, schema) => (id) => ({
  [CALL_API]: {
    types,
    endpoint: `${base}/${id}`,
    options: {
      method: 'DELETE'
    },
    schema
  }
});