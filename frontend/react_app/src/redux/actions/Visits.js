import { CALL_API } from '../middleware/api';

import { VISIT_LIST_SCHEMA, STATUS_SCHEMA } from '../schema';

import {
  fetchAllFactory,
  putFactory,
  postFactory,
  deleteFactory
} from './utils';

export const GET_VISITS_REQUEST = 'GET_VISITS_REQUEST';
export const GET_VISITS_SUCCESS = 'GET_VISITS_SUCCESS';
export const GET_VISITS_FAILURE = 'GET_VISITS_FAILURE';

export const VISITS_FOR_CLIENT_REQUEST = 'VISITS_FOR_CLIENT_REQUEST';
export const VISITS_FOR_CLIENT_SUCCESS = 'VISITS_FOR_CLIENT_SUCCESS';
export const VISITS_FOR_CLIENT_FAILURE = 'VISITS_FOR_CLIENT_FAILURE';

export const PUT_VISIT_REQUEST = 'PUT_VISIT_REQUEST';
export const PUT_VISIT_SUCCESS = 'PUT_VISIT_SUCCESS';
export const PUT_VISIT_FAILURE = 'PUT_VISIT_FAILURE';

export const POST_VISIT_REQUEST = 'POST_VISIT_REQUEST';
export const POST_VISIT_SUCCESS = 'POST_VISIT_SUCCESS';
export const POST_VISIT_FAILURE = 'POST_VISIT_FAILURE';

export const DELETE_VISIT_REQUEST = 'DELETE_VISIT_REQUEST';
export const DELETE_VISIT_SUCCESS = 'DELETE_VISIT_SUCCESS';
export const DELETE_VISIT_FAILURE = 'DELETE_VISIT_FAILURE';

const BASE = '/visits';

const fetchVisits = fetchAllFactory(
  [GET_VISITS_REQUEST, GET_VISITS_SUCCESS, GET_VISITS_FAILURE],
  BASE,
  VISIT_LIST_SCHEMA
);

export const loadVisits = (where) => (dispatch) => {
  return dispatch(fetchVisits(where));
};

const fetchVisitsForClient = (clientId) => ({
  [CALL_API]: {
    types: [VISITS_FOR_CLIENT_REQUEST,
            VISITS_FOR_CLIENT_SUCCESS,
            VISITS_FOR_CLIENT_FAILURE],
    endpoint: `/visits?where="client_id: ${clientId}"`,
    schema: VISIT_LIST_SCHEMA
  }
});

export const loadVisitsForClient = (clientId) => (dispatch) => {
  return dispatch(fetchVisitsForClient(clientId));
};

const putVisit = putFactory(
  [PUT_VISIT_REQUEST, PUT_VISIT_SUCCESS, PUT_VISIT_FAILURE],
  BASE,
  VISIT_LIST_SCHEMA
);

const postVisit = postFactory(
  [POST_VISIT_REQUEST, PUT_VISIT_SUCCESS, PUT_VISIT_FAILURE],
  BASE,
  VISIT_LIST_SCHEMA
);

export const saveVisit = (id, visit) => (dispatch) => {
  return id ?
    dispatch(putVisit(id, visit)) :
    dispatch(postVisit(visit));
};

export const deleteVisit = deleteFactory(
  [DELETE_VISIT_REQUEST, DELETE_VISIT_SUCCESS, DELETE_VISIT_FAILURE],
  BASE,
  STATUS_SCHEMA
);