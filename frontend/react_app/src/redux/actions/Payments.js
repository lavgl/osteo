import {
  fetchAllFactory,
  putFactory,
  postFactory,
  deleteFactory
} from './utils';

import { PAYMENT_LIST_SCHEMA, STATUS_SCHEMA } from '../schema';

export const GET_PAYMENTS_REQUEST = 'GET_PAYMENTS_REQUEST';
export const GET_PAYMENTS_SUCCESS = 'GET_PAYMENTS_SUCCESS';
export const GET_PAYMENTS_FAILURE = 'GET_PAYMENTS_FAILURE';

export const PUT_PAYMENT_REQUEST = 'PUT_PAYMENT_REQUEST';
export const PUT_PAYMENT_SUCCESS = 'PUT_PAYMENT_SUCCESS';
export const PUT_PAYMENT_FAILURE = 'PUT_PAYMENT_FAILURE';

export const POST_PAYMENT_REQUEST = 'PUT_PAYMENT_REQUEST';
export const POST_PAYMENT_SUCCESS = 'PUT_PAYMENT_SUCCESS';
export const POST_PAYMENT_FAILURE = 'PUT_PAYMENT_FAILURE';

export const DELETE_PAYMENT_REQUEST = 'DELETE_PAYMENT_REQUEST';
export const DELETE_PAYMENT_SUCCESS = 'DELETE_PAYMENT_SUCCESS';
export const DELETE_PAYMENT_FAILURE = 'DELETE_PAYMENT_FAILURE';

const BASE = '/payments';

const fetchPayments = fetchAllFactory(
  [GET_PAYMENTS_REQUEST, GET_PAYMENTS_SUCCESS, GET_PAYMENTS_FAILURE],
  BASE,
  PAYMENT_LIST_SCHEMA
);

export const loadPayments = (where) => (dispatch) => {
 return dispatch(fetchPayments(where));
};

export const putPayment = putFactory(
  [PUT_PAYMENT_REQUEST, PUT_PAYMENT_SUCCESS, PUT_PAYMENT_FAILURE],
  BASE,
  PAYMENT_LIST_SCHEMA
);

export const postPayment = postFactory(
  [PUT_PAYMENT_REQUEST, PUT_PAYMENT_SUCCESS, PUT_PAYMENT_FAILURE],
  BASE,
  PAYMENT_LIST_SCHEMA
);

export const savePayment = (id, payment) => (dispatch) => {
  return id ?
    dispatch(putPayment(id, payment)) :
    dispatch(postPayment(payment));
};

export const deletePayment = deleteFactory(
  [DELETE_PAYMENT_REQUEST, DELETE_PAYMENT_SUCCESS, DELETE_PAYMENT_FAILURE],
  BASE,
  STATUS_SCHEMA
);