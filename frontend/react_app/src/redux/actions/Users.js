import {
  USER_LIST_SCHEMA,
  STATUS_SCHEMA
} from '../schema';

import {
  fetchAllFactory,
  fetchOneFactory,
  putFactory,
  postFactory,
  deleteFactory
} from './utils';

export const PUT_AGENT_REQUEST = 'PUT_AGENT_REQUEST';
export const PUT_AGENT_FAILURE = 'PUT_AGENT_FAILURE';

export const POST_USER_REQUEST = 'POST_USER_REQUEST';

export const USERS_REQUEST = 'USERS_REQUEST';
export const USERS_SUCCESS = 'USERS_SUCCESS';
export const USERS_FAILURE = 'USERS_FAILURE';

export const USER_REQUEST = 'USER_REQUEST';
export const USER_SUCCESS = 'USER_SUCCESS';
export const USER_FAILURE = 'USER_FAILURE';

export const DELETE_USER_REQUEST = 'DELETE_USER_REQUEST';
export const DELETE_USER_SUCCESS = 'DELETE_USER_SUCCESS';
export const DELETE_USER_FAILURE = 'DELETE_USER_FAILURE';

const BASE = '/users';

const fetchUsers = fetchAllFactory(
  [USERS_REQUEST, USERS_SUCCESS, USERS_FAILURE],
  BASE,
  USER_LIST_SCHEMA
);

export const loadUsers = (where) => (dispatch) => {
  return dispatch(fetchUsers(where));
};

const fetchUser = fetchOneFactory(
  [USER_REQUEST, USER_SUCCESS, USER_FAILURE],
  BASE,
  USER_LIST_SCHEMA
);

export const loadUser = (id) => (dispatch, getState) => {
  const state = getState();
  const user = state.Entities.getIn(['users', id]);
  if (!user) {
    return dispatch(fetchUser(id));
  }
};

const putUser = putFactory(
  [PUT_AGENT_REQUEST, USERS_SUCCESS, PUT_AGENT_FAILURE],
  BASE,
  USER_LIST_SCHEMA
);

const postUser = postFactory(
  [POST_USER_REQUEST, USERS_SUCCESS, USERS_FAILURE],
  BASE,
  USER_LIST_SCHEMA
);

export const saveUser = (id, user) => (dispatch) => {
  return id ?
    dispatch(putUser(id, user)) :
    dispatch(postUser(user));
};

export const deleteUser = deleteFactory(
  [DELETE_USER_REQUEST, DELETE_USER_SUCCESS, DELETE_USER_FAILURE],
  BASE,
  STATUS_SCHEMA
);