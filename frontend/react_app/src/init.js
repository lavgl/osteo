import React from 'react';
import { render } from 'react-dom';
import { browserHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
import Root from './containers/Root';
import configureStore from './store/configureStore';
import injectTapEventPlugin from 'react-tap-event-plugin';
import './index.css';

window.React = React;

injectTapEventPlugin();

const store = configureStore();
const history = syncHistoryWithStore(browserHistory, store);

const initWithRoutes = (routes) => render(
  <Root store={store} history={history} routes={routes} />,
  document.getElementById('app')
);

export default initWithRoutes;
