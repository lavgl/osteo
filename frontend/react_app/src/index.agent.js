import initWithRoutes from './init';
import routes from './routes/agent';

initWithRoutes(routes);