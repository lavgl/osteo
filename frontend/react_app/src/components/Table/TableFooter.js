import React, { Component, PropTypes } from 'react';

import IconButton from 'material-ui/IconButton';
import ChevronLeft from 'material-ui/svg-icons/navigation/chevron-left';
import ChevronRight from 'material-ui/svg-icons/navigation/chevron-right';

import './TableFooter.css';

class TableFooter extends Component {
  static propTypes = {
    page: PropTypes.number,
    perPage: PropTypes.number,
    size: PropTypes.number,
    pages: PropTypes.number,
    nextPage: PropTypes.func,
    prevPage: PropTypes.func
  };

  render() {
    const { page, perPage, size, pages } = this.props;
    const from = page * perPage + 1;
    const to = (page + 1) * perPage;
    return (
      <tr>
        <td>
          <div className="table-footer__container">
            <div>
              {`${from}-${to < size ? to : size} of ${this.props.size}`}
            </div>
            <IconButton
              onClick={this.props.prevPage}
              disabled={page === 0}>
              <ChevronLeft />
            </IconButton>
            <IconButton
              onClick={this.props.nextPage}
              disabled={page === pages - 1}>
              <ChevronRight />
            </IconButton>
          </div>
        </td>
      </tr>
    )
  }
}

export default TableFooter;