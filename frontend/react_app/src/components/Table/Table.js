import React, { Component, PropTypes } from 'react';
import Immutable from 'immutable';
import {
  Table, TableBody, TableHeader,
  TableHeaderColumn,TableRow,
  TableRowColumn, TableFooter
} from 'material-ui/Table';

import { default as Footer } from './TableFooter';

import { calculateData, calculatePages } from './utils';

import './Table.css';

const DEFAULT_PAGE = 0;
const DEFAULT_ROWS_PER_PAGE = 10;

class MyTable extends Component {
  static propTypes = {
    data: PropTypes.instanceOf(Immutable.List),
    columns: PropTypes.array
  };

  constructor(props) {
    super(props);

    this.state = {
      visibleData: calculateData(DEFAULT_ROWS_PER_PAGE, DEFAULT_PAGE, props.data),
      page: DEFAULT_PAGE,
      rowsPerPage: DEFAULT_ROWS_PER_PAGE,
      pages: calculatePages(DEFAULT_ROWS_PER_PAGE, props.data)
    };

    this.nextPage = this.nextPage.bind(this);
    this.prevPage = this.prevPage.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    const { rowsPerPage, page } = this.state;
    const { data } = nextProps;

    if (this.props.data === data) return;

    const pages = calculatePages(rowsPerPage, data);
    const newPage = page > pages ? pages : page;
    const visibleData = calculateData(rowsPerPage, newPage, data);

    this.setState({
      visibleData,
      pages,
      page: newPage
    });
  }

  nextPage() {
    const { data } = this.props;
    const { rowsPerPage } = this.state;
    const nextPage = this.state.page + 1;

    if (nextPage >= this.state.pages) return;

    const visibleData = calculateData(rowsPerPage, nextPage, data);
    const pages = calculatePages(rowsPerPage, data);

    this.setState({
      page: nextPage,
      visibleData,
      pages
    });
  }

  prevPage() {
    const { data } = this.props;
    const { rowsPerPage } = this.state;
    const prevPage = this.state.page - 1;

    if (prevPage < 0) return;

    const visibleData = calculateData(rowsPerPage, prevPage, data);
    const pages = calculatePages(rowsPerPage, data);

    this.setState({
      page: prevPage,
      visibleData,
      pages
    });
  }

  render() {
    return (
      <Table selectable={false}>
        <TableHeader
          displaySelectAll={false}
          adjustForCheckbox={false}
        >
          <TableRow>
            {this.props.columns.map(c =>
              <TableHeaderColumn key={c.name} className="table-header-column">
                {c.displayName}
              </TableHeaderColumn>
            )}
          </TableRow>
        </TableHeader>
        <TableBody
          displayRowCheckbox={false}>
          {this.state.visibleData.map((datum, i)=> (
            <TableRow key={i}>
              {this.props.columns.map((c, i) => (
                <TableRowColumn key={i} className="table-row-column">
                  {c.render ? c.render(datum, c, this.props, this.state) : datum.get(c.name)}
                </TableRowColumn>
              ))}
            </TableRow>
          ))}
        </TableBody>
        <TableFooter>
          <Footer
            page={this.state.page}
            perPage={this.state.rowsPerPage}
            size={this.props.data.size}
            nextPage={this.nextPage}
            prevPage={this.prevPage}
            pages={this.state.pages}
          />
        </TableFooter>
      </Table>
    )
  }
}

export default MyTable;