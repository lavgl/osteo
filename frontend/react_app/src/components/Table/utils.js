export const calculateData = (perPage, page, data) => {
  const from = perPage * page;
  const to = perPage * (page + 1);

  return data.filter((v, i) => i >= from && i < to);
};

export const calculatePages = (perPage, data) => {
  return Math.ceil(data.size / perPage);
};

export const filterData = (data, columns, filterString, props) => {
  if (filterString === '') return data;

  const normalizedFilterString = filterString.toLowerCase();

  return data.filter(datum => {
    return columns
      .filter(c => c.filter)
      .some(c => {
        const value = (c.value && c.value(datum, c, props)) ||  datum.get(c.name);
        const normalizedValue = (''+value).toLowerCase();
        return normalizedValue.indexOf(normalizedFilterString) !== -1;
      });
  });
};