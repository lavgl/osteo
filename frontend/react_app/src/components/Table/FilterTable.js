import React, { Component, PropTypes } from 'react';
import Immutable from 'immutable';

import TextField from 'material-ui/TextField';

import Table from './Table';

import { filterData } from './utils';

import './FilterTable.css';

const DEFAULT_FILTER_STRING = '';

class FilterTable extends Component {
  static propTypes = {
    data: PropTypes.instanceOf(Immutable.List),
    columns: PropTypes.array
  };

  constructor(props) {
    super(props);

    this.state = {
      filterString: DEFAULT_FILTER_STRING,
      filteredData: filterData(props.data, props.columns, DEFAULT_FILTER_STRING)
    };

    this.handleChange = this.handleChange.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    const { columns } = this.props;
    const { filterString } = this.state;
    const { data } = nextProps;

    if (this.props.data === data) return;

    const filteredData = filterData(data, columns, filterString);

    this.setState({ filteredData });
  }

  handleChange(e) {
    const { data, columns } = this.props;

    const filterString = e.target.value;
    const filteredData = filterData(data, columns, filterString, this.props);

    this.setState({ filterString,  filteredData });
  }

  render() {
    return (
      <div>
        <div className="filter-table__filter__wrapper">
          <TextField
            value={this.state.filterString}
            onChange={this.handleChange}
            floatingLabelText="Search"
          />
        </div>
        <Table {...this.props} data={this.state.filteredData} columns={this.props.columns} />
      </div>
    )
  }
}

export default FilterTable;