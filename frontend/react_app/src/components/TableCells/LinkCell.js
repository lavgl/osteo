import React, { PropTypes } from 'react';
import { Link } from 'react-router';

const style = {
  width: '100%',
  height: '100%'
};

export const LinkCell = (props) => (
  <Link to={props.to}>
    <div style={style}>
      {props.label}
    </div>
  </Link>
);

LinkCell.propTypes = {
  to: PropTypes.string.isRequired,
  label: PropTypes.any.isRequired
};