import React, { Component, PropTypes } from 'react';
import { findDOMNode } from 'react-dom';

import Popover from 'material-ui/Popover';
import Paper from 'material-ui/Paper';
import IconButton from 'material-ui/IconButton';
import DeleteIcon from 'material-ui/svg-icons/action/delete';
import RaisedButton from 'material-ui/RaisedButton';

class RemoveButton extends Component {
  static propTypes = {
    deleteFn: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);

    this.state = {
      open: false
    };

    this.toggle = this.toggle.bind(this);
    this.remove = this.remove.bind(this);
  }

  toggle() {
    this.setState({ open: !this.state.open });
  }

  remove() {
    this.toggle();
    this.props.deleteFn();
  }

  render() {
    const popover = (
      <Popover
        open={this.state.open}
        anchorEl={this.button}
        onRequestClose={this.toggle}
        targetOrigin={{ vertical: 'bottom', horizontal: 'right' }}
        anchorOrigin={{ vertical: 'top', horizontal: 'middle' }}
      >
        <Paper>
          <h3 style={{ margin: 10 }}>Are you sure?</h3>
          <RaisedButton
            label="Yes"
            primary={true}
            onClick={this.remove}
            fullWidth={true}
          />
          <RaisedButton
            label="No"
            secondary={true}
            onClick={this.toggle}
            fullWidth={true}
          />
        </Paper>
      </Popover>
    );

    return (
      <IconButton ref={(ref) => this.button = findDOMNode(ref)}
                  onClick={this.toggle}>
        <DeleteIcon />
        {popover}
      </IconButton>
    );
  }
}

export { RemoveButton };