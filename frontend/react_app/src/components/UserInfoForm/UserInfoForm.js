import React, { Component, PropTypes } from 'react';
import Immutable from 'immutable';

import { Form } from 'formsy-react';

import RaisedButton from 'material-ui/RaisedButton';

import { FormsyText } from 'formsy-material-ui';

import { identity } from '../../utils';

const errorMessages = {
  wordsError: 'Please only use letters',
  numericError: 'Please provide a number',
  emailError: 'Please provide a valid, unique email',
  required: 'Field is required',
  maxLength: 'Value should be shorter',
  minLength: 'Value should be longer'
};

class UserInfoForm extends Component {
  static propTypes = {
    user: PropTypes.instanceOf(Immutable.Map),
    onFieldChange: PropTypes.func,
    onReset: PropTypes.func,
    onSubmit: PropTypes.func,
    additionalFields: PropTypes.array
  };

  static defaultProps = {
    user: Immutable.Map(),
    onFieldChange: identity,
    onReset: identity,
    onSubmit: identity
  };

  constructor(props) {
    super(props);

    this.state = {
      canSubmit: false
    };

    this.enableButton = this.enableButton.bind(this);
    this.disableButton = this.disableButton.bind(this);
  }

  enableButton() {
    this.setState({ canSubmit: true });
  }

  disableButton() {
    this.setState({ canSubmit: false });
  }

  render() {
    const { user, onFieldChange, additionalFields } = this.props;
    return (
      <Form
        onSubmit={this.handleSubmit}
        onValid={this.enableButton}
        onInvalid={this.disableButton}
      >
        <div>
          <FormsyText
            name="name"
            floatingLabelText="Name"
            value={user.get('name') || ""}
            onChange={onFieldChange.bind(this, 'name')}
            validationError={errorMessages.maxLength}
            validations="maxLength:64"
          />
        </div>

        <div>
          <FormsyText
            name="username"
            floatingLabelText="Username *"
            value={user.get('username') || ""}
            onChange={onFieldChange.bind(this, 'username')}
            required
          />
        </div>

        <div>
          <FormsyText
            name="mail"
            floatingLabelText="Mail *"
            value={user.get('mail') || ""}
            onChange={onFieldChange.bind(this, 'mail')}
            validations={{
              isEmail: true,
              maxLength: 64
            }}
            required
            validationError={errorMessages.emailError}
          />
        </div>

        <div>
          <FormsyText
            name="phone"
            floatingLabelText="Phone"
            value={user.get('phone') || ""}
            onChange={onFieldChange.bind(this, 'phone')}
          />
        </div>

        <div>
          <FormsyText
            name="address"
            floatingLabelText="Address"
            value={user.get('address') || ""}
            onChange={onFieldChange.bind(this, 'address')}
            multiLine={true}
          />
        </div>

        <div>
          <FormsyText
            name="description"
            floatingLabelText="Description"
            value={user.get('description') || ""}
            onChange={onFieldChange.bind(this, 'description')}
            multiLine={true}
          />
        </div>

        {additionalFields && additionalFields.map((field, i) => <div key={i}>{field}</div>)}

        <div style={{
          fontSize: 12,
          color: 'red',
          marginBottom: 20,
          marginTop: 20
        }}>
          * - required fields
        </div>
        <div>
          <RaisedButton
            label="Reset"
            secondary={true}
            onClick={this.props.onReset}
          />
          <RaisedButton
            label="Submit"
            primary={true}
            onClick={this.props.onSubmit}
            disabled={!this.state.canSubmit}
          />
        </div>

      </Form>
    );
  }
}

export default UserInfoForm;