import React, { Component, PropTypes } from 'react';

import I18n from '../../decorators/I18n';
import AppBarMUI from 'material-ui/AppBar';

import sun from '../../../public/assets/sun.svg';

@I18n('header', {
  enableSetLocale: true,
  enableGetLocale: true
})
class AppBar extends Component {
  static propTypes = {
    t: PropTypes.func,
    setLocale: PropTypes.func,
    getLocale: PropTypes.func
  };

  render() {
    const { t, setLocale, getLocale } = this.props;
    return (
      <AppBarMUI
        title = {t('title')}
        iconElementLeft = {
          <img
            className="appbar-sunny-icon"
            src={sun}
            alt=""
            onClick={() => {
              setLocale(getLocale() === 'en' ? 'ru' : 'en')
            }
            }
          />}
      />
    )
  }
}

export default AppBar;