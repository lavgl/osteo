import React, { Component, PropTypes } from 'react';
import { browserHistory } from 'react-router';

import { Toolbar, ToolbarGroup, ToolbarSeparator } from 'material-ui/Toolbar';
import FlatButton from 'material-ui/FlatButton';

import I18n from '../../decorators/I18n';

@I18n('navigation')
class Navigation extends Component {
  static propTypes = {
    t: PropTypes.func
  };

  handleLink(to) {
    browserHistory.push(to);
  }

  render() {
    const { t } = this.props;
    return (
      <Toolbar>
        <ToolbarGroup firstChild={true}>
          <FlatButton
            label = {t('agentsList')}
            onClick={this.handleLink.bind(null, '/')}
          />
          <ToolbarSeparator />
          <FlatButton
            label = {t('clientsList')}
            onClick={this.handleLink.bind(null, '/clients')}
          />
        </ToolbarGroup>
      </Toolbar>
    )
  }
}

export default Navigation;