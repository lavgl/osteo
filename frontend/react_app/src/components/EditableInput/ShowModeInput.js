import React, { PropTypes } from 'react';

const ShowModeInput = (props) => (
  <div>{props.value}</div>
);

export default ShowModeInput;

ShowModeInput.propTypes = {
  value: PropTypes.any.isRequired
};