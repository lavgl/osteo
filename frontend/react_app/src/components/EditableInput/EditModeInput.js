import React, { Component, PropTypes } from 'react';
import { findDOMNode } from 'react-dom';

import TextField from 'material-ui/TextField';
import IconButton from 'material-ui/IconButton';
import DoneIcon from 'material-ui/svg-icons/action/done';
import ClearIcon from 'material-ui/svg-icons/content/clear';


class EditModeInput extends Component {
  static propTypes = {
    value: PropTypes.any.isRequired,
    onSubmit: PropTypes.func.isRequired,
    onCancel: PropTypes.func.isRequired
  };

  static defaultProps = {
    onCancel: () => {}
  };

  constructor(props) {
    super(props);

    this.state = {
      value: props.value
    };

    this.setValue = this.setValue.bind(this);
    this.submit = this.submit.bind(this);
  }

  componentDidMount() {
    const node = findDOMNode(this);
    const input = node.querySelector('input');
    input.focus();
  }

  setValue(e) {
    this.setState({ value: e.target.value });
  }

  submit(e) {
    e.preventDefault();
    this.props.onSubmit(this.state.value);
  }

  render() {
    const { onCancel, ...rest } = this.props;

    return (
      <form onSubmit={this.submit}>
        <TextField {...rest}
          value={this.state.value}
          onChange={this.setValue}
        />
        <IconButton onClick={this.submit}>
          <DoneIcon />
        </IconButton>
        <IconButton onClick={onCancel.bind(this, this.state.value)}>
          <ClearIcon />
        </IconButton>
      </form>
    );
  }
}

export default EditModeInput;