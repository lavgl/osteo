import React, { Component, PropTypes } from 'react';

import EditModeInput from './EditModeInput';
import ShowModeInput from './ShowModeInput';

// TODO: add some indications this could be edited. go to showMode on focus lost
// fix width
class EditableInput extends Component {
  static propTypes = {
    type: PropTypes.string,
    value: PropTypes.any.isRequired,
    onChange: PropTypes.func.isRequired,
    onCancel: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);

    this.state = {
      edit: false
    };

    this.toggleMode = this.toggleMode.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
  }

  toggleMode() {
    this.setState({ edit: !this.state.edit });
  }

  handleSubmit(value) {
    this.props.onSubmit(value, this.props.value);
    this.toggleMode();
  }

  handleCancel(value) {
    if (this.props.onCancel) {
      this.props.onCancel(value, this.props.value);
    }
    this.toggleMode();
  }

  render() {
    const EditMode = (
      <div>
        <EditModeInput {...this.props}
          value={this.props.value}
          onSubmit={this.handleSubmit}
          onChange={this.props.onChange}
          onCancel={this.handleCancel}
        />
      </div>
    );

    const ShowMode = (
      <div
        style={{ cursor: 'pointer' }}
        onClick={this.toggleMode}
      >
        <ShowModeInput
          value={this.props.value}
        />
      </div>
    );

    return this.state.edit
      ? EditMode
      : ShowMode;
  }
}

export default EditableInput;