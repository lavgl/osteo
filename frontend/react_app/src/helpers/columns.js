import React from 'react';

import {
  renderLinkByIdFactory,
  deleteButtonFactory
} from './renderFactories';

const agentIdLink = renderLinkByIdFactory(
  (agent) => `/agents/${agent.get('id')}`,
  (agent) => agent.get('id')
);

const renderClientNumber = (datum, column, { clients }) => {
  const value = column.value(datum, column, { clients });
  return <span>{value || 0}</span>;
};

const deleteUserButton = deleteButtonFactory('deleteUser', 'showNotification');
const deleteVisitButton = deleteButtonFactory('deleteVisit', 'showNotification');
const deletePaymentButton = deleteButtonFactory('deletePayment', 'showNotification');

const renderLinkClientById = renderLinkByIdFactory(
  (datum) => `/clients/${datum.get('id')}`,
  (datum) => datum.get('id')
);

const renderVisitCount = (datum, column, { visits }) => {
  const value = column.value(datum, column, { visits });
  return <span>{value || 0}</span>
};

const renderPaymentsAmount = (datum, column, { payments }) => {
  const value = column.value(datum, column, { payments });
  return <span>{value || 0}</span>
};

export const agentTableColumns = [
  { name: 'id', displayName: 'Id', filter: true, render: agentIdLink },
  { name: 'name', displayName: 'Name', filter: true },
  { name: 'username', displayName: 'Username', filter: true },
  { name: 'phone', displayName: 'Phone', filter: true },
  { name: 'mail', displayName: 'Mail', filter: true },
  { name: 'address', displayName: 'Address', filter: true },
  {
    name: 'clientsNumber',
    displayName: 'Clients',
    render: renderClientNumber,
    filter: true,
    value: (datum, column, { clients }) => {
      return clients && clients.filter(c => c.get('agent_id') === datum.get('id')).size;
    }
  },
  {
    name: 'paymentsAmount',
    displayName: 'Paid',
    render: renderPaymentsAmount,
    filter: true,
    value: (datum, column, { payments }) => {
      return payments && payments
          .filter(p => p.get('agent_id') === datum.get('id'))
          .reduce((result, p) => result + p.get('amount'), 0);

    }
  },
  { name: 'deleteUser', displayName: 'Delete', render: deleteUserButton }
];

export const clientTableColumns = [
  { name: 'id', displayName: 'Id', filter: true, render: renderLinkClientById },
  { name: 'name', displayName: 'Name', filter: true },
  { name: 'username', displayName: 'Username', filter: true },
  { name: 'phone', displayName: 'Phone', filter: true },
  { name: 'mail', displayName: 'Mail', filter: true },
  { name: 'address', displayName: 'Address', filter: true },
  { name: 'description', displayName: 'Description', filter: true },
  {
    name: 'visitsCount',
    displayName: 'Visits',
    filter: true,
    render:renderVisitCount,
    value: (datum, column, { visits }) => {
      return visits && visits.filter(v => v.get('client_id') === datum.get('id')).size;
    }
  },
  { name: 'deleteClient', displayName: 'Delete', render: deleteUserButton }
];

export const visitsTableColumns = [
  { name: 'id', displayName: 'Id', filter: true },
  { name: 'date', displayName: 'Date', filter: true },
  { name: 'time', displayName: 'Time', filter: true, render: () => 'defined at ClientInfoPage'},
  { name: 'deleteVisit', displayName: 'Remove', render: deleteVisitButton }
];

export const paymentsTableColumns = [
  { name: 'id', displayName: 'Id', filter: true },
  { name: 'amount', displayName: 'Amount', filter: true },
  { name: 'date', displayName: 'Date', filter: true },
  { name: 'time', displayName: 'Time', filter: true },
  { name: 'deletePayment', displayName: 'Delete', render: deletePaymentButton }
];