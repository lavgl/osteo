import React from 'react';

import { LinkCell, RemoveButton} from '../components/TableCells';

export const renderLinkByIdFactory = (toFn, labelFn) => (datum, column, props) => {
  return (
    <LinkCell
      to={toFn(datum, column, props)}
      label={labelFn(datum, column, props)}
    />
  );
};

export const deleteButtonFactory = (deleteFnName, showNotificationFnName = 'showNotification') =>
  (datum, column, props) => {
    const deleteFn = props[deleteFnName];
    const showNotification = props[showNotificationFnName];

    if (!deleteFn) {
      console.error(`ERROR! deleteFn property doesn't exist. Column: ${column.name}`);
    }

    if (!showNotification) {
      console.error(`ERROR! showNotification prop doesn't exist. Column: ${column.name}`);
    }

    const deleteFnWrapper = () => {
      return deleteFn(datum.get('id'))
        .then(result => {
          showNotification(!result.error
            ? 'Removed successfully'
            : 'Error while removing');
          return result;
        });
    };

    return (
      <RemoveButton
        deleteFn={deleteFnWrapper}
      />
    );
  };