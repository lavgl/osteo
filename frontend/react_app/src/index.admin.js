import initWithRoutes from './init';
import routes from './routes/admin';

initWithRoutes(routes);