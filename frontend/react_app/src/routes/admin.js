import React from 'react';
import { Route, IndexRoute } from 'react-router';
import App from '../containers/App';

import AgentsListPage from '../pages/AgentsListPage/AgentsListPage';
import AgentInfoPage from '../pages/AgentInfoPage';
import ClientInfoPage from '../pages/ClientInfoPage';
import ClientsListPage from '../pages/ClientsListPage';
import createUserPageFabric from '../pages/CreateUserPage';

export default (
  <Route path="/" component={App}>
    <IndexRoute component={AgentsListPage} />
    <Route path="agents/new" component={createUserPageFabric({ role: 'Agent' })} />
    <Route path="clients/new" component={createUserPageFabric({ role: 'Client' })}/>
    <Route path="agents/:id" component={AgentInfoPage}/>
    <Route path="clients" components={ClientsListPage}/>
    <Route path="clients/:id" components={ClientInfoPage}/>
  </Route>
);