import React, { Component, PropTypes } from 'react';

import get from 'lodash/get';
import map from 'lodash/map';
import filter from 'lodash/filter';
import pick from 'lodash/pick';
import has from 'lodash/has';
import first from 'lodash/first';

const I18n = (namespaces, { enableSetLocale, enableGetLocale } = {}) => (WrappedComponent) => {
  class I18nConsumer extends Component {
    static contextTypes = {
      i18n: PropTypes.object
    };

    constructor(props, context) {
      super(props, context);

      this.unsubscribe = context.i18n.subscribe(() => this.forceUpdate());
      this.translate = this.translate.bind(this);
      this.setLocale = this.setLocale.bind(this);
      this.getLocale = this.getLocale.bind(this);
    }

    componentWillUnmount() {
      this.unsubscribe();
    }

    translate(label) {
      const { i18n } = this.context;
      return getTranslation(i18n.getLocalization(), namespaces, label);
    }

    setLocale(locale) {
      this.context.i18n.setLocale(locale);
    }

    getLocale() {
      return this.context.i18n.getLocale();
    }

    render() {
      return (
        <WrappedComponent
          {...this.props}
          t = {this.translate}
          setLocale = {enableSetLocale ? this.setLocale : undefined}
          getLocale = {enableGetLocale ? this.getLocale : undefined}
        />
      );
    }
  }

  return I18nConsumer;
};

export default I18n;

function getTranslation(localization, namespaces, label) {
  const providedNamespaces = pick(localization, namespaces);

  if (has(providedNamespaces, label)) {
    return process(localization, get(providedNamespaces, label));
  }

  const translations = filter(map(providedNamespaces, ns => get(ns, label)));

  if (translations.length > 1) {
    throw new Error(
      `Too many translations! You should either provide
      less namespaces in class definition, or define namespace 
      for label definition. Example: 'header.${label}'`);
  }

  if (!translations.length) {
    throw new Error(`No translations for label ${label}!`)
  }

  return process(localization, first(translations));
}

const PLACEHOLDER_REGEXP = /^\{\{[\w\d.]+}}$/;
const extractPath = placeholder => placeholder.slice(2, -2);

function process(localization, translationOrPlaceholder) {
  const isPlaceholder = PLACEHOLDER_REGEXP.test(translationOrPlaceholder);

  return isPlaceholder
    ? get(localization, extractPath(translationOrPlaceholder))
    : translationOrPlaceholder;
}