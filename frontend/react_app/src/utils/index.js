export const extractOwnUserInfo = user => {
  const fields = ['id', 'name', 'username', 'phone', 'mail', 'address', 'description'];

  return fields.reduce((res, field) => {
    res[field] = user[field];
    return res;
  }, {});
};

export const identity = (arg) => arg;