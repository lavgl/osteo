import React, { Component, PropTypes } from 'react';

import remove from 'lodash/remove';
import get from 'lodash/get';

import localization from '../utils/localization.json';

const DEFAULT_LOCALE = 'en';

class I18nState {
  constructor(localization, defaultLocale) {
    this.localization = localization;
    this.locale = defaultLocale;
    this.subscriptions = [];
  }

  setLocale(locale) {
    this.locale = locale;
    this.subscriptions.forEach(f => f());
  }

  getLocale() {
    return this.locale;
  }

  getLocalization() {
    return get(this.localization, this.locale,
      get(this.localization, DEFAULT_LOCALE))
  }

  subscribe(f) {
    this.subscriptions.push(f);
    return () => this.unsubscribe(f);
  }

  unsubscribe(f) {
    remove(this.subscriptions, s => s === f);
  }
}

class I18nProvider extends Component {
  static propTypes = {
    defaultLocale: PropTypes.string.isRequired,
    children: PropTypes.node.isRequired
  };

  static defaultProps = {
    defaultLocale: DEFAULT_LOCALE
  };

  static childContextTypes = {
    i18n: PropTypes.object
  };

  constructor(props) {
    super(props);

    this.i18n = new I18nState(localization, props.defaultLocale);
  }

  getChildContext() {
    return {
      i18n: this.i18n
    };
  }

  render() {
    return (
      <div>{this.props.children}</div>
    )
  }
}

export default I18nProvider;