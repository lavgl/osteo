import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import Immutable from 'immutable';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import { amber600, amber700, amber400, grey50, white } from 'material-ui/styles/colors';
import typography from 'material-ui/styles/typography';

import Snackbar from 'material-ui/Snackbar';

import AppBar from '../components/AppBar';
import Navigation from '../components/Navigation';

import I18nProvider from './I18nProvider';

import { hideNotification } from '../redux/actions/Notifications';
import { loadUsers } from '../redux/actions/Users';

import sun from '../../public/assets/sun.svg';

const muiTheme = getMuiTheme({
  palette: {
    primary1Color: amber600,
    primary2Color: amber700,
    primary3Color: amber400,
    pickerHeaderColor: amber700
  },
  raisedButton: {
    fontWeight: typography.fontWeightNormal
  },
  flatButton: {
    fontWeight: typography.fontWeightNormal
  },
  toolbar: {
    backgroundColor: grey50
  },
  snackbar: {
    backgroundColor: amber700,
    actionColor: white
  }
});

function mapStateToProps(state) {
  return {
    notifications: state.Notifications.get('notifications')
  };
}

const mapDispatchToProps = {
  hideNotification,
  loadUsers
};

@connect(mapStateToProps, mapDispatchToProps)
class App extends Component {
  static propTypes = {
    notifications: PropTypes.instanceOf(Immutable.List).isRequired,
    hideNotification: PropTypes.func.isRequired,
    loadUsers: PropTypes.func.isRequired,
    // loadUser: PropTypes.func.isRequired,
    children: PropTypes.object.isRequired,
  };

  componentWillMount() {
    this.props.loadUsers();
  }

  render() {
    const { notifications } = this.props;
    return (
      <MuiThemeProvider muiTheme={muiTheme}>
        <I18nProvider defaultLocale="en">
          <AppBar />
          <Navigation />
          <div>
            {this.props.children}
          </div>
          {notifications && notifications.size ? (
            <Snackbar
              open={!!this.props.notifications.size}
              message={this.props.notifications.first()}
              autoHideDuration={5000}
              onRequestClose={this.props.hideNotification}
              action={
                <img className="snackbar-sunny-icon" src={sun} alt="" />
              }
            />
          ) : null}
        </I18nProvider>
      </MuiThemeProvider>
    );
  }
}

export default App;
