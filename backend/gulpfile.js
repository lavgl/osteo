const gulp = require('gulp');
const defineTask = require('../gulp/utils').defineTaskCreator(gulp);
const config = require('../config')();

defineTask('backend:copySrc', './tasks/copy', {
  src: config.backend.src.all,
  dest: config.backend.dest.serverDir
});