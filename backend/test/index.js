require('src/injectPaths')();

var Minimist = require('minimist');
var Mocha = require('mocha');
var Glob = require('glob');

const argv = Minimist(process.argv.slice(2));

const pattern = '{src/**/*.spec.js,test/**/*.spec.js}';

const mocha = new Mocha({
  grep: argv.grep
});

Glob(
  pattern,
  {},
  (err, files) => {
    if (err) {
      console.error(err);
      return;
    }
    files.forEach(file => mocha.addFile(file));
    mocha.run(failures => {
      process.on('exit', () => {
        process.exit(failures);
      });
    });
  }
);