const expect = require('chai').expect;
const mysql = require('mysql');

var db = require('./ConnectionWrapper');

describe('ConnectionWrapper', () => {
  describe('getConnection method', () => {
    beforeEach(() => {
      db.getConnection();
    });

    afterEach(() => {
      db.closeConnection();
    });

    it('should create connection after first call', () => {
      expect(db.connection).to.be.defined;
    });

    it('should not recreate connection after second call', () => {
      const connection = db.connection;

      db.getConnection();
      db.getConnection();

      expect(db.connection).to.be.equal(connection);
    });
  });
});