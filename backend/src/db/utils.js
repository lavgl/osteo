const str = require('sqlstring');

const db = require('db');

module.exports.parseWhere = parseWhere;

const startsWith = (value, str) => {
  return value.slice(0, str.length) === str;
};

function parseWhere(where) {
    var result = '',
      value, clause;
    for (var key in where) {
      value = where[key];
      clause = key;
      if (typeof(value) === 'number') clause = key + ' = ' + value;
      else if (typeof(value) === 'string') {
        /**/ if (startsWith(value, '>=')) clause = key + ' >= ' + str.escape(value.substring(2));
        else if (startsWith(value, '<=')) clause = key + ' <= ' + str.escape(value.substring(2));
        else if (startsWith(value, '<>')) clause = key + ' <> ' + str.escape(value.substring(2));
        else if (startsWith(value, '>' )) clause = key + ' > '  + str.escape(value.substring(1));
        else if (startsWith(value, '<' )) clause = key + ' < '  + str.escape(value.substring(1));
        else if (startsWith(value, '(' )) clause = key + ' IN (' + value.substr(1, value.length-2).split(',').map(function(s) { return str.escape(s); }).join(',') + ')';
        else if (value.indexOf('..') !== -1) {
          value = value.split('..');
          clause = '(' + key + ' BETWEEN ' + str.escape(value[0]) + ' AND ' + str.escape(value[1]) + ')';
        } else if ((value.indexOf('*') !== -1) || (value.indexOf('?') !== -1)) {
          value = value.replace(/\*/g, '%').replace(/\?/g, '_');
          clause = key + ' LIKE ' + str.escape(value);
        } else clause = key + ' = ' + str.escape(value);
      }
      if (result) result = result + ' AND ' + clause; else result = clause;
    }
    return result;
};