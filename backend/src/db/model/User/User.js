const hasha = require('hasha');
const db = require('db');

const generateModel = require('db/model/generateModel').generateModel;

const { random } = require('utils/string');

const table = 'Users';

const User = generateModel({
  table
});

User.create = create;

module.exports = User;

function create(obj) {
  const password = obj.password || random(8);
  obj.password = hasha(password).slice(8);

  return new Promise((resolve, reject) => {
    const sql = 'INSERT INTO ?? SET ?';
    db.getConnection().then(connection => {
      connection.query(sql, [ table , obj ], (err, result) => {
        if (err) {
          console.error(err);
          reject(err);
        }
        resolve(result);
      });
    });
  });
}