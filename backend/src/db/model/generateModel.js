const db = require('db');
const parseWhere = require('db/utils').parseWhere;

module.exports.generateModel = generateModel;

function generateModel({ table }) {
  class GeneratedModel {
    getAll() {
      return new Promise((resolve, reject) => {
        db.getConnection().then(connection => {
          connection.query('SELECT * FROM ?? WHERE deleted=false', [table], (err, result) => {
            if (err) {
              console.error(err);
              reject(err);
            }
            resolve(result);
          });
        })
      });
    }

    getById(id) {
      return new Promise((resolve, reject) => {
        db.getConnection().then(connection => {
          connection.query('SELECT * FROM ?? WHERE id = ? AND deleted=false', [table, id], (err, result) => {
            if (err) {
              console.error(err);
              reject(err);
            }
            resolve(result);
          });
        });
      });
    }

    getWhere(where) {
      const cond = parseWhere(where);
      const sql = 'SELECT * FROM ?? WHERE ' + cond + ' and deleted=false';

      return new Promise((resolve, reject) => {
        db.getConnection().then(connection => {
          connection.query(sql, [table], (err, result) => {
            if (err) {
              console.error(err);
              reject(err);
            }
            resolve(result);
          });
        });
      });
    }

    getOneWhere(where) {
      return this.getWhere(where)
        .then(items => {
          if (items.length === 0) return null;

          if (!Array.isArray(items)) {
            console.log('array should be here');
            items = [items];
          }

          return items[0];
        })
        .catch(err => {
          console.log('error here!', err.stack);
          return err;
        });
    }

    create(obj) {
      return new Promise((resolve, reject) => {
        const sql = 'INSERT INTO ?? SET ?';
        db.getConnection().then(connection => {
          connection.query(sql, [ table, obj ], (err, result) => {
            if (err) {
              console.error(err);
              reject(err);
            }
            resolve(result);
          });
        });
      });
    }

    update(where, obj) {
      return new Promise((resolve, reject) => {
        const sql = 'UPDATE ?? SET ? WHERE ' + parseWhere(where);
        db.getConnection().then(connection => {
          connection.query(sql, [ table, obj ], (err, result) => {
            if (err) {
              console.error(err);
              reject(err);
            }
            resolve(result);
          });
        });
      });
    }

    updateById(id, obj) {
      return this.update({ id }, obj);
    }

    remove(where) {
      return new Promise((resolve, reject) => {
        const sql = 'UPDATE ?? SET deleted=true WHERE ' + parseWhere(where);
        db.getConnection().then(connection => {
          connection.query(sql, [table], (err, result) => {
            if (err) {
              console.error(err);
              reject(err);
            }
            resolve(result);
          });
        });
      });
    }

    removeById(id) {
      return this.remove({ id });
    }
  }

  return new GeneratedModel();
}