const generateModel = require('db/model/generateModel').generateModel;

module.exports = generateModel({
  table: 'Visits'
});