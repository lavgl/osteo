var mysql = require('mysql');
var config = require('config')();

const defaultOptions = config.backend.db;
var wrapper = {};
var cache = {
  options: {}
};

var connection;

function getConnection(options) {
  const opts = Object.assign({}, defaultOptions, options);

  if (!connection) {
    // TODO: use connection pool
    return createConnection(opts);
  }

  if (connection && !options) {
    return Promise.resolve(connection);
  }

  // TODO: implement reconnection when getCOnnection called
  // with different parameters

  cache.options = opts;
}

function closeConnection() {
  if (connection) {
    connection.end((err) => {
      if (err) {
        console.error('disconnection error');
      }
    });
  }
}

function createConnection(options) {
  connection = mysql.createConnection(options);

  connection.on('error', function (err) {
    console.log('db error', err);
    if (err.code === 'PROTOCOL_CONNECTION_LOST') {
      createConnection(options);
    } else {
      throw err;
    }
  });

  return new Promise((resolve, reject) => {
    connection.connect(err => {
      if (err) {
        console.log('error while connecting', err);
        reject(err);
      }
      resolve(connection);
    })
  });
}

wrapper.getConnection = getConnection;
wrapper.closeConnection = closeConnection;

module.exports = wrapper;