const router = require('koa-router')();
const User = require('db/model/User');
const serializeUser = require('utils/serializeUser');

router.post('/auth', authPost);
router.get('/auth', authGet);

module.exports = router.routes();

function* authPost(next) {
  // TODO: replace password with some aggregated hash
  const { username, password } = this.request.body;

  if (!username || !password) {
    this.throw(400);
  }

  const user = yield User.getOneWhere({ username });

  if (!user || password !== user.password) {
    this.throw(401);
  }

  this.session.token = serializeUser(user);

  this.set('Location', 'users');
  this.status = 301;
}

function* authGet() {
  // common way to use this is redirection from register api
  const { userId } = this.session;

  if (typeof userId !== 'number') this.throw('User Id should be number');

  const user = yield User.getById(userId);
  if (!user) this.throw('user should be found here!');

  this.session = {
    token: serializeUser(user)
  };

  this.set('Location', 'users');
  this.status = 301;
}