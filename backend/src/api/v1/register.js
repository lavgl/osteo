const router = require('koa-router')();
const User = require('db/model/User');

const msg = require('utils/messages');

router.post('/register', register);

module.exports = router.routes();

function* register() {
  const { body } = this.request;
  const { mail, username, password } = body;

  if (!mail) this.throw(400, msg.mailIsRequired());
  if (!username) this.throw(400, msg.usernameIsRequired());
  if (!password) this.throw(400, msg.passwordIsRequired());

  var user = yield User.getOneWhere({ username });
  if (user) this.throw(400, msg.usernameIsNotUnique());

  user = yield User.getOneWhere({ mail });
  if (user) this.throw(400, msg.mailIsNotUnique());

  const info = yield User.create(body);

  if (!info || info.failedCount > 0 || !info.insertId) {
    this.throw(400, msg.internalServerError());
  }

  this.session.userId = info.insertId-0;

  this.set('Location', 'auth');
  this.status = 301;
}