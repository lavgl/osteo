const router = require('koa-router')();
const config = require('config')().backend.server;
const sendEmail = require('utils/emailSender');

router.post('/orders', postOrder);

module.exports = router.routes();

function* postOrder() {
  const { body } = this.request;

  const response = yield sendEmail(body);
  console.log('Result of sendidg mail', response);

  const redirectUrl = `http://${config.host}:${config.port}/redirect.html`;
  this.redirect(redirectUrl);
  this.status = 301;
}