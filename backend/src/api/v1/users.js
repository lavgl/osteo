const router = require('koa-router')();
const User = require('db/model/User');
const api = require('api/generateAPI').generateAPI({ model: User });
const utils = require('api/utils');

router.get('/users', api.get);
router.get('/users/:id', api.get);
router.put('/users/:id', api.put);
router.post('/users', api.post);
router.delete('/users/:id', api.delete);

module.exports = router.routes();