var router = require('koa-router')();
const Payment = require('db/model/Payment');

const mappers = {
  date: (date) => new Date(date)
};

const api = require('api/generateAPI').generateAPI({ model: Payment, mappers });

router.get('/payments', api.get);
router.post('/payments', api.post);
router.put('/payments/:id', api.put);
router.delete('/payments/:id', api.delete);

module.exports = router.routes();