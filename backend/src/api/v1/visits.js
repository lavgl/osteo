const router = require('koa-router')();
const Visit = require('db/model/Visit');

const mappers = {
  date: (date) => new Date(date)
};

const api = require('api/generateAPI').generateAPI({ model: Visit, mappers });

router.get('/visits', api.get);
router.post('/visits', api.post);
router.put('/visits/:id', api.put);
router.delete('/visits/:id', api.delete);

module.exports = router.routes();