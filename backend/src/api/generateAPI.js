module.exports.parseQueryString = parseQueryString;
module.exports.generateAPI = generateAPI;

// getById introduced to fetch agents
function generateAPI({ model, refetch = model.getWhere, mappers }) {

  function* getEntity(next) {
    let { qs, where, err } = parseQueryString(this.request.querystring);

    if (err) {
      this.status = 500;
      this.body = err.message;
      return;
    }

    const id = this.params && this.params.id;

    if (id) {
      where = Object.assign({}, where, { id });
    }

    // format for where is
    // where="id: 2, username: uniq2"
    const items = where ?
      yield model.getWhere(where) :
      yield model.getAll();

    if (!items) this.status = 204;
    else this.body = items;
  }

  function* postEntity(next) {
    let { body } = this.request;
    if (mappers) {
      body = Object.keys(body).reduce((result, key) => {
        const fn = mappers[key];
        result[key] = fn ? fn(body[key]) : body[key];
        return result;
      }, {});
    }
    const result = yield model.create(body);
    if (!result.code) {
      this.status = 201;
      const id = result.insertId;
      this.body = yield refetch.call(model, { id });
    } else {
      this.status = 500;
      this.body = result.error;
    }
  }

  function* putEntity(next) {
    let { body } = this.request;
    const id = this.params.id - 0;
    if (mappers) {
      body = Object.keys(body).reduce((result, key) => {
        const fn = mappers[key];
        result[key] = fn ? fn(body[key]) : body[key];
        return result;
      }, {});
    }
    const result = yield model.updateById(id, body);
    if (!result.code) {
      this.status = 200;
      this.body = yield refetch.call(model, { id });
    } else {
      this.status = 500;
      this.body = result.error;
    }
  }

  function* deleteEntity(next) {
    const id = this.params.id - 0;
    const result = yield model.removeById(id);
    if (!result.code) {
      this.status = 200;
      this.body = {
        status: 'OK',
        id
      }
    } else {
      this.status = 500;
      this.body = result.error;
    }
  }

  return {
    'get': getEntity,
    post: postEntity,
    put: putEntity,
    'delete': deleteEntity
  };
}

function parseQueryString(queryString) {
  const result = {};

  const qs = queryString.split('&')
    .reduce((result, entry) => {
      let [ key, value ] = entry.split('=');
      if (/\[]/.test(key)) {
        key = key.replace('[]', '');
        value = value.split(',');
      }
      result[key] = value;
      return result;
    }, {});

  var where;
  var err;

  try {
    where = qs.where ? parseWhere(qs.where) : null;
  } catch (e) {
    err = e;
  }

  return {
    qs,
    err,
    where
  };
}

const parseWhere = (where) => {
  try {
    const string = JSON.parse(decodeURIComponent(where));
    const json = string.split(',').reduce((result, conditionEntry) => {
      const [key, value] = conditionEntry.split(':');
      result[key.trim()] = value.trim();
      return result;
    }, {});
    return json;
  } catch (e) {
    console.log(e);
    throw e;
  }
};