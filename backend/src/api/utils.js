module.exports.unique = unique;
module.exports.extractClientIds = extractClientIds;
module.exports.flatten = flatten;
module.exports.mergeClientsIntoAgentsInfo = mergeClientsIntoAgentsInfo;

function unique(value, index, array) {
  for (let i = 0; i < index; i++) {
    if (value === array[i]) return false;
  }
  return true;
}

function extractClientIds(agentsInfo) {
  return agentsInfo.reduce((res, agent) => {
    const { visits } = agent;
    const clientIds = visits.map(v => v.clientId).filter(unique);
    res[agent.id] = clientIds;
    return res;
  }, {});
}

function flatten(obj) {
  return Object.keys(obj).reduce((res, key) => {
    const value = obj[key];
    return res.concat(value).filter(unique);
  }, []);
}

function mergeClientsIntoAgentsInfo(agentsInfo, clientIds, clients) {
  const info = agentsInfo.map(agent => {
    const _clients = clientIds[agent.id].map(clientId => {
      return clients.find(c => c.id === clientId);
    });
    return Object.assign({}, agent, { clients: _clients });
  });

  return info;
}