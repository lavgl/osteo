const router = require('koa-router')();

const API_V1_PATH = '/api/v1';

const visitsApi = require('api/v1/visits');
const paymentsApi = require('api/v1/payments');
const usersApi = require('api/v1/users');
const authApi = require('api/v1/auth');
const registerApi = require('api/v1/register');
const ordersApi = require('api/v1/orders');

router.use(API_V1_PATH, authApi);
router.use(API_V1_PATH, registerApi);
router.use(API_V1_PATH, visitsApi);
router.use(API_V1_PATH, paymentsApi);
router.use(API_V1_PATH, usersApi);
router.use(API_V1_PATH, ordersApi);

module.exports = router.routes();

