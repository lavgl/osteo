const path = require('path');
const modulePath = require('app-module-path');

module.exports = injectPaths;

function injectPaths() {
  require('app-module-path/register');
  const [ root, backend ] = buildRootPath('osteo');
  modulePath.addPath(root);
  modulePath.addPath(backend);
  console.log('injected paths: ', __dirname, root, backend);
}

const buildRootPath = (projectName, backend = 'backend') => {
  const preRoot = path.resolve(projectName).split(path.sep);
  const index = preRoot.findIndex(unit => unit === projectName);
  const backendIndex = preRoot.findIndex(unit => unit === backend);
  return [
    preRoot.slice(0, index).concat(projectName).join(path.sep),
    preRoot.slice(0, backendIndex).join(path.sep)
  ];
};