module.exports = function serializeUser(user) {
  const { username, role } = user;
  return { username, role };
};