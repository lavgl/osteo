module.exports.paymentMapper = paymentMapper;
module.exports.visitMapper = visitMapper;
module.exports.userMapper = userMapper;

function paymentMapper(payment) {
  //console.log('payment', payment);
  return {
    id: payment.payment_id,
    amount: payment.amount,
    date: payment.payment_date,
    visitId: payment.visit_id
  };
}

function visitMapper(visit) {
  //console.log('visit', visit);
  return {
    id: visit.visit_id,
    clientId: visit.client_id,
    date: visit.visit_date
  };
}

function userMapper(user) {
  //console.log('user', user);
  return {
    id: user.id,
    name: user.name,
    mail: user.mail,
    address: user.address,
    username: user.username,
    phone: user.phone,
    description: user.description
  };
}