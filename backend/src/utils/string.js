module.exports.random = (length = 8) => {
  const generated = generate();
  if (length > generated.length) {
    console.error('YOU NEED TO REWORK RANDOM STRING GENERATOR');
  }
  return generated.substring(0, length);
};

const generate = () => Math.random().toString(36).slice(2);