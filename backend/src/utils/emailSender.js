const config = require('config')();
const emailConfig = config.backend.email;

const {
  publicKey,
  privateKey,
  fromEmail,
  fromName,
  recipient
} = emailConfig;

const mailjet = require ('node-mailjet').connect(publicKey, privateKey);

module.exports = sendEmail;

function sendEmail(data) {
  const mappedData = dataMapper(data);

  return mailjet
    .post('send')
    .request(buildRequest(mappedData))
    .then((response, body) => {
      console.log('Email successfully sent. Body:', response);
      return { response, body };
    })
    .catch((err, response) => {
      console.log('Error while sending email:', err);
      return { err, response };
    });
}

const massageTypeMapper = {
  Type: 'Не указан',
  Osteopathy: "Остеопатия",
  Classical: "Классический массаж",
  Honey: "Медовый массаж",
  Thai: "Тайский массаж",
  Craniosacral: "Кранио-сакральная терапия"
};

function dataMapper(data) {
  return {
    name: data.name || "",
    type: massageTypeMapper[data.type] || "",
    comment: data.comments || "",
    phoneOrEmail: data.mail || ""
  };
}

function buildRequest(data) {
  const { name, type, comment, phoneOrEmail } = data;
  return {
    "FromEmail": fromEmail,
    "FromName": fromName,
    "Subject": buildSubject({ type, name, phoneOrEmail }),
    "Text-part": buildMessage(data),
    "Recipients": buildRecipients(recipient)
  }
}

function buildSubject({ type, name, phoneOrEmail }) {
  return `${name} - ${type} - ${phoneOrEmail}`;
}

function buildMessage(data) {
  const { name, type, phoneOrEmail, comment } = data;
  return (
    `
    Заявка на массаж:
    Имя: ${name};
    Тип массажа: ${type};
    Контакт: ${phoneOrEmail};
    Комментарий:
    ${comment}
`);
}

function buildRecipients(recipient) {
  return [{
    "Email": recipient.toString()
  }];
}
