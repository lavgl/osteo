const mailIsRequired = (args) => `email should be provided`;
const usernameIsRequired = (args) => `username is required`;
const passwordIsRequired = (args) => `password is required`;

const mailIsNotUnique = (args) => `email is not unique`;
const usernameIsNotUnique = (args) => `username is not unique`;

const internalServerError = (args) => `Internal server error`;

module.exports = {
  mailIsRequired,
  usernameIsRequired,
  passwordIsRequired,
  mailIsNotUnique,
  usernameIsNotUnique,
  internalServerError
};