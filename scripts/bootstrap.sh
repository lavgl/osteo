#!/usr/bin/env bash

apt-get update

# install nvm
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.32.1/install.sh | bash

#install node
nvm install 7
nvm alias default 7

#install mysql
wget http://dev.mysql.com/get/mysql-apt-config_0.6.0-1_all.deb # TODO: find appropriate place
sudo dpkg -i mysql-apt-config_0.6.0-1_all.deb
sudo apt-get update
apt-get install mysql-server
sudo mysql_secure_installation
mysql --version
sudo mysql_install_db

service mysql status


sudo mysql -uroot -proot -e 'create database osteo';

# bootstrap database
bash ./setup_database.sh

# create osteo user for database

sudo mysql -uroot -proot -e 'create user "osteo"@"localhost"; grant all privileges on osteo.* to "osteo"@"localhost"';

# install dependencies