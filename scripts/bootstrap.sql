DROP TABLE IF EXISTS Payments;
DROP TABLE IF EXISTS Visits;
DROP TABLE IF EXISTS Users;

# DROP DATABASE IF EXISTS osteo;
# CREATE DATABASE IF NOT EXISTS osteo CHARACTER SET 'utf8mb4';

# TODO: add index to username
CREATE TABLE `Users` (
  id BIGINT PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(255) DEFAULT '',
  mail VARCHAR(255) UNIQUE DEFAULT '',
  address VARCHAR(255) DEFAULT '',
  username VARCHAR(255) NOT NULL UNIQUE,
  phone VARCHAR(255) DEFAULT '',
  role ENUM('Client', 'Agent', 'Admin') DEFAULT 'Agent',
  description VARCHAR(255) DEFAULT '',
  password VARCHAR(255) NOT NULL,
  trusted BOOLEAN DEFAULT FALSE,
  deleted BOOLEAN DEFAULT FALSE,
  agent_id BIGINT,
  FOREIGN KEY (agent_id) REFERENCES Users(ID)
) CHARACTER SET 'utf8mb4';

CREATE TABLE Visits (
  id BIGINT PRIMARY KEY AUTO_INCREMENT,
  client_id BIGINT NOT NULL,
  date TIMESTAMP DEFAULT NOW(),
  deleted BOOLEAN DEFAULT FALSE,
  FOREIGN KEY (client_id) REFERENCES Users(id)
) CHARACTER SET 'utf8mb4';

CREATE TABLE Payments (
  id BIGINT PRIMARY KEY AUTO_INCREMENT,
  agent_id BIGINT NOT NULL,
  amount INT CHECK (amount > 0),
  date TIMESTAMP DEFAULT NOW(),
  visit_id BIGINT,
  deleted BOOLEAN DEFAULT FALSE,
  FOREIGN KEY (agent_id) REFERENCES Users(id),
  FOREIGN KEY (visit_id) REFERENCES Visits(id)
) CHARACTER SET 'utf8mb4';

INSERT INTO Users (name, mail, address, username, phone, role, description, password, trusted)
VALUES ('a', 'b', 'c', 'uniq1', 'd', 'Agent', 'e', 'f', TRUE);
INSERT INTO Users (name, mail, address, username, phone, role, description, password)
VALUES ('a', 'c', 'c', 'uniq2', 'd', 'Agent', 'e', 'f');
INSERT INTO Users (name, mail, address, username, phone, role, description, password, agent_id)
VALUES ('a', 'd', 'c', 'uniq3', 'd', 'Client', 'e', 'f', 1);
INSERT INTO Users (name, mail, address, username, phone, role, description, password, trusted, agent_id)
VALUES ('a', 'e', 'c', 'uniq4', 'd', 'Client', 'e', 'f', TRUE, 1);
INSERT INTO Users (name, mail, address, username, phone, role, description, password, trusted, agent_id)
VALUES ('a', 'f', 'c', 'uniq5', 'd', 'Client', 'e', 'f', TRUE, 2);
INSERT INTO Users (name, mail, address, username, phone, role, description, password, trusted)
VALUES ('a', 'g', 'c', 'uniq6', 'd', 'Admin', 'e', 'f', TRUE);

INSERT INTO Visits (client_id) VALUES (3);
INSERT INTO Visits (client_id) VALUES (3);
INSERT INTO Visits (client_id) VALUES (3);
INSERT INTO Visits (client_id) VALUES (4);
INSERT INTO Visits (client_id) VALUES (5);
INSERT INTO Visits (client_id) VALUES (4);

INSERT INTO Payments (agent_id, amount, visit_id) VALUES (2, 10000, 1);
INSERT INTO Payments (agent_id, amount) VALUES (2, 20000);