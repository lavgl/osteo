const path = require('path');
const env = process.env.NODE_ENV;

// TODO: separate gulp config files

module.exports = () => {
  return {
    "frontend": {
      "landing": {},
      "app": {}
    },
    "backend": {
      "db": {
        "host": "localhost",
        "user": "osteo",
        "password": "",
        "database": env !== 'test' ? 'osteo' : 'osteo_test',
        "charset": "utf8mb4"
      },
      "server": {
        "host": env === 'production' ? '146.185.176.42' :'localhost',
        "port": env === 'production' ? 80 : 3004
      },
      "email": {
        "publicKey": "c794156680d08cd24c8c67d15cb87723",
        "privateKey": "bfcb72eecbf8e82199d017319bd2332f",
        "fromEmail": "avg.idm@yandex.ru",
        "fromName": "Заявка на массаж",
        "recipient": "avg.idm@gmail.com"
      }
    }
  }
};